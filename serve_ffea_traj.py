#!/usr/bin/env python
# Copyright (c) Intangible Realities Lab, University Of Bristol. All rights reserved.
# Licensed under the GPL. See License.txt in the project root for license information.
"""
Send the frames of a pre-computed FFEA trajectory to Narupa.

FFEA simulates proteins as meshes using finite element analysis. It can be
obtained at <https://ffea.bitbucket.io>.

Defining a system with a `.ffea` file
-------------------------------------

A FFEA trajectory is defined in a ``.ffea`` file. Such file is a pseudo-XML file
that contains the simulation parameters and the path to the relevant files.

A `.ffea` file looks like (this is just an extract and is likely not fully valid):

::
    <param>
        <restart = 0>
        <dt = 1e-15>
        <trajectory_out_fname = trajectory.ftj>
    </param>

    <system>
        <blob>
            <conformation>
                <motion_state = DYNAMIC>
                <nodes = nodes.node>
                <surface = surface.surf>
            </conformation>
            <scale = 5e-9>
            <centroid_pos = (-1.05,0.0,0.0)>
            <rotation = (-1.,0.,0.,0.,1.,0.,0.,0.,1.)>
        </blob>
    </system>

From this file, this script reads the path of the trajectory file from the
``trajectory_out_fname`` tag, and the definition of the blobs. A "blob" is one
mesh in a system. It the initial position of the vertices is described in the
`.node` file pointed to by the `nodes` tag. The edges of the surface faces of
the mesh are described in the `.surf` file pointed to by the `surface` tag. The
initial position of a blob centroid in the system, and its initial rotation,
are specified in the `centroid_pos` and `rotation` tag, respectively. The
`scale` keyword is used to scale the coordinates of the vertices to nanometers.

The value of the `centroid_pos` tag is a vector positioning the centroid of the
blob in the system.

.. note::

    The unit of the `centroid_pos` tag is unclear, but appears to be nanometers.
    (See <https://bitbucket.org/FFEA/ffea/issues/35>.)

The value of the `rotation` tag can be a set of 3 Euler angles, or a flatten
3x3 rotation matrix.

.. warning::

    The `rotation` keyword is not read in the current version.

.. warning::

    Coordinates can be scaled with the `scale` keyword. They can be further
    scaled with the `calc_compress` and the `compress` keywords. Only the
    `scale`d is read in the current version of narupa-ffea.

.. warning::

    A `.ffea` file can specify more than one conformation for a blob. Each
    conformation has its own set of edges and vertices, and the blob can switch
    from one of these sets to another during the course of the trajectory.

    Narupa-ffea only supports one conformation per blob in its current version.


The documentation of the format can be found at
<https://ffea.readthedocs.io/en/stable/ffea_iFile.html> and
<https://ffea.readthedocs.io/en/stable/keywordReference.html>.

Specify the topology of a blob with `.node` and `.surf` files
-------------------------------------------------------------

The initial coordinates of a blob in a given conformation are read from a
`.node` file. The file contains the coordinates of the vertices that are at the
surface or inside the mesh in two different section. We only read the vertices
that are on the surface. Internally, these vertices are exposed as particles.

The connectivity of the mesh surface is described in a `.surf` file. The edges
of the mesh are read as bonds.

.. note::

    Future versions will read face triplets in addition to bond pairs.

The documentation for these file format is available at
<https://ffea.readthedocs.io/en/stable/ioFiles.html>.

Trajectory file
---------------

The actual trajectory is read from a `.ftj` file. That file contains the
coordinate of each node of each blob for each recorded simulation frame.

.. warning::

    A frame can be DYNAMIC of STATIC. In the former case, the coordinates are
    updated, while they are not in the latter case. Only dynamic frames are
    supported.

.. warning::

    When more than one conformation is defined for a given blob, a simulation
    frame can switch from one conformation to an other. This is not supported
    in narupa-ffea.
"""

import argparse
from typing import Iterable, Iterator, Dict, List, NamedTuple, Tuple, Any
import itertools
import collections
import re
from pathlib import Path
from narupa.trajectory import FrameData, FrameServer
from narupa.core.timing import yield_interval

PositionList = List[List[float]]
BondList = List[List[int]]


class BlobDefinition(NamedTuple):
    """
    Required information to build a blob.
    """
    nodes: Path
    surface: Path
    scale: float
    centroid_pos: List[float]


def stripped_lines(lines: Iterable[str]) -> Iterator[str]:
    """
    Yield the lines of an iterable (e.g. a file) after stripping them.
    """
    for line in lines:
        yield line.strip()


def serve_ffea(path: Path) -> None:
    """
    Loop over the frames of a FFEA simulation and serve them to Narupa.
    """
    with FrameServer() as frame_server:
        with open(path) as infile:
            trajectory_out_fname, definition = read_ffea_file(
                infile, base_path=path)
        frame_data = frame_data_from_definitions(definition)
        keep_per_blob = count_surface_node_per_blob(frame_data)
        frame_server.send_frame(0, frame_data)
        with open(trajectory_out_fname) as infile:
            infinite_frames = itertools.cycle(
                read_trajectory(infile, keep_per_blob=keep_per_blob))
            numbered_interval_frame = enumerate(
                zip(yield_interval(1/120), infinite_frames), start=1)
            for frame_index, (_, frame) in numbered_interval_frame:
                frame_server.send_frame(frame_index, frame)


def count_surface_node_per_blob(frame: FrameData) -> List[int]:
    """
    Count the number of vertices per blob from a :class:`FrameData`.

    The vertices reported in a :class:`FrameData` are only the surface ones,
    and each blob is reported as a different residue. Getting the number of
    vertices per residue therefore means getting the number of vertices at the
    surface of each blob.

    The frame must contain the residue information.
    """
    count = collections.Counter(frame.particle_residues)
    return [item[1] for item in sorted(count.items())]


def read_ffea_file(infile: Iterable[str],
                   base_path: Path = Path('.')) -> Tuple[Path, List[BlobDefinition]]:
    """
    Read, from ``.ffea`` file, what is needed to serve a trajectory.

    The function reads the path to the `.ftj` file describing the trajectory.
    It also reads the initial definition of all the blobs.
    """
    re_tag = re.compile(r'<\s*(?P<key>/?[a-zA-Z0-9_]+)(\s*=\s*(?P<value>[^>]*))?\s*>')
    blobs = []
    template_blob = {
        'nodes': None,
        'surface': None,
        'centroid_pos': None,
        'scale': 1e-10,
    }
    current_blob: Dict[str, Any] = {}
    trajectory_out_fname = None

    for line_number, line in enumerate(infile, start=1):
        if not line.strip():
            continue
        tag = re_tag.search(line)
        if tag is None:
            raise IOError(f'Line {line_number} seems invalid.')
        key = tag.group('key')
        value = tag.group('value')
        
        if key == 'trajectory_out_fname':
            trajectory_out_fname = _relative_to_config_path(Path(value), base_path)
        elif key == 'blob':
            current_blob = template_blob.copy()
            blobs.append(current_blob)
        elif key == '/blob':
            current_blob = {}
        elif key in ('nodes', 'surface'):
            current_blob[key] = _relative_to_config_path(Path(value), base_path)
        elif key == 'scale':
            current_blob['scale'] = float(value)
        elif key == 'centroid_pos':
            assert value[0] == '(' and value[-1] == ')'
            unbracketed = value[1:-1]
            current_blob['centroid_pos'] = list(map(float, unbracketed.split(',')))

    if trajectory_out_fname is None:
        raise ValueError('No trajectory file defined in the input.')

    return trajectory_out_fname, [BlobDefinition(**blob) for blob in blobs]


def node_file_to_positions(infile: Iterable[str]) -> PositionList:
    """
    Read the position of the outer nodes from a `.node` file.

    The coordinates are in whatever unit the file uses. They are not scaled
    nor rotated.
    """
    lines = stripped_lines(infile)
    metadata = _read_node_header(lines)
    surface_nodes = []
    for line in lines:
        if line == 'interior nodes:':
            break
        surface_nodes.append(list(map(float, line.split())))
    assert len(surface_nodes) == metadata['num_surface_nodes'], (
        'Inconsistent number of surface nodes.')
    # We do not need the rest of the file so we stop reading here.
    return surface_nodes


def compute_centroid(vertices: PositionList) -> List[float]:
    """
    Compute the center of geometry of a collection of points.
    """
    if not vertices:
        raise ValueError('No positions provided.')
    sums = [0.0] * len(vertices[0])
    for vertex in vertices:
        for index, coordinate in enumerate(vertex):
            sums[index] += coordinate
    return [value / len(vertices) for value in sums]


def translate_to(vertices: PositionList, destination: List[float]) -> PositionList:
    """
    Translate a set of points so that its center of geometry is at the destination.
    """
    centroid = compute_centroid(vertices)
    vector = [d - c for d, c in zip(destination, centroid)]
    return [
        [vertex_i + vector_i for vertex_i, vector_i in zip(vertex, vector)]
        for vertex in vertices
    ]


def scale_positions(vertices: PositionList, scale: float) -> PositionList:
    """
    Apply a scaling factor to a collection of points without changing its center.
    """
    centroid = compute_centroid(vertices)
    vertices = translate_to(vertices, [0.0, 0.0, 0.0])
    vertices = [
        [coordinate * scale for coordinate in vertex]
        for vertex in vertices
    ]
    vertices = translate_to(vertices, centroid)
    return vertices


def surf_file_to_bonds(infile: Iterator[str]) -> BondList:
    """
    Read the connectivity of an outer mesh from a `.surf` file.

    Return a collection of bond, each of them being an edge of the mesh
    represented as a pair of indices. The indices start at 0.
    """
    lines = stripped_lines(infile)
    header = next(lines)
    assert header == 'ffea surface file', 'The input does not appear to be a surf file.'
    # skip the rest of the header
    for _ in itertools.takewhile(lambda x: x != 'faces:', lines):
        pass

    edges: List[List[int]] = []
    for line in lines:
        nodes = list(map(int, line.split()[1:]))
        for index_in_face in range(-1, len(nodes) - 1):
            edges.append([nodes[index_in_face], nodes[index_in_face + 1]])
    return edges


def offset_bonds(bonds: BondList, offset: int) -> BondList:
    """
    Add a given offset to all the indices in a collection of bond pairs.
    """
    return [[index + offset for index in bond] for bond in bonds]


def blob_from_definition(definition: BlobDefinition) -> Tuple[PositionList, BondList]:
    """
    Read the vertices and edges of a blob, scaled and translated.

    If the input files are correct, then the coordinates are expressed in meters.
    """
    with open(definition.surface) as infile:
        bonds = surf_file_to_bonds(infile)
    with open(definition.nodes) as infile:
        nodes = node_file_to_positions(infile)
    nodes = scale_positions(nodes, definition.scale)
    if definition.centroid_pos is not None:
        nodes = translate_to(nodes, definition.centroid_pos)
    return nodes, bonds


def frame_data_from_definitions(
        definitions: List[BlobDefinition], pretend_element: int = 12) -> FrameData:
    """
    Build a full system from blob definitions. Return a :class:`FrameData`.

    Coordinates are expressed in nanometers.
    """
    all_nodes: PositionList = []
    all_bonds: BondList = []
    residue_indices = []
    for blob_index, definition in enumerate(definitions):
        nodes, bonds = blob_from_definition(definition)
        bonds = offset_bonds(bonds, len(all_nodes))
        all_nodes.extend(nodes)
        all_bonds.extend(bonds)
        residue_indices.extend([blob_index] * len(nodes))
    
    # Unit is meters, we want nanometer
    all_nodes = scale_positions(all_nodes, 1e9)

    frame_data = FrameData()
    frame_data.particle_positions = all_nodes
    frame_data.particle_count = len(all_nodes)
    frame_data.particle_elements = [pretend_element] * len(all_nodes)
    frame_data.particle_residues = residue_indices
    frame_data.residue_chains = range(len(definitions))
    frame_data.bond_pairs = all_bonds
    return frame_data


def read_trajectory(infile: Iterable[str], keep_per_blob: List[int]) -> Iterator[FrameData]:
    """
    Yield the frames of a trajectory as :class:`FrameData` instances.

    A trajectory file contains the coordinates for all the vertices, including
    the ones that are not at the surface. The vertices on the surface, though,
    appear first for each blob. The `keep_per_blob` argument is a list of
    the number of vertices to use for each blob.
    """
    lines = stripped_lines(infile)
    header = next(lines)
    message = 'The input does not appear to be a trajectory.'
    assert header == 'FFEA_trajectory_file', message
    
    # Skip header
    _consume_until_value(lines, '*')

    blobs: List[PositionList] = []
    current_blob: PositionList = []
    for line in lines:
        if line.startswith('Blob') :
            # Line format is
            # Blob X, Conformation X, step X
            conformation_id = int(line.split(',')[1].split()[1])
            if conformation_id != 0:
                message = 'Only one conformation per blob is supported at the moment.'
                raise NotImplementedError(message)
            current_blob = []
            blobs.append(current_blob)
        elif line == 'DYNAMIC':
            pass
        elif line == 'STATIC':
            raise NotImplementedError('Static frames are not yet supported.')
        elif line == '*':
            vertices = []
            for blob, keep in zip(blobs, keep_per_blob):
                vertices.extend(blob[:keep])
            # Positions are in meter, we want nanometers
            vertices = scale_positions(vertices, 1e9)
            yield frame_data_from_positions(vertices)
            blobs = []
        elif line == 'Conformation Changes:':
            _consume_until_value(lines, '*')
        elif '\x00' in line:
            continue
        else:
            current_blob.append([float(coordinate) for coordinate in line.split()[:3]])


def frame_data_from_positions(positions: PositionList) -> FrameData:
    """
    Generate a :class:`FrameData` from a collection of vertex positions.
    """
    frame_data = FrameData()
    frame_data.particle_positions = positions
    return frame_data


def _consume_until_value(iterator: Iterator, value: Any) -> None:
    """
    Consume the elements of an iterator until a given sentinel value.

    The element with the sentinel value is consumed as well. Comparisons are
    made on the basis of equality.
    """
    for _ in itertools.takewhile(lambda x: x != value, iterator):
        pass


def _relative_to_config_path(path, config_path):
    """
    Return a path relative to another file path, or an absolute path.
    """
    if path.is_absolute():
        return path
    return config_path.parent / path


def _read_node_header(lines: Iterator[str]) -> Dict[str, int]:
    """
    Read the node counts in the header of a `.node` file and check it is
    consistent.
    """
    header = next(lines)
    assert header == 'ffea node file', 'The input does not appear to be a node file.'
    metadata: Dict[str, int] = {}
    for line in lines:
        if line == 'surface nodes:':
            break
        key, value = line.split()
        metadata[key] = int(value)
    else:  # no break
        raise AssertionError('No surface node found in the file.')
    assert metadata['num_surface_nodes'] > 0, 'Input file does not define surface node.'
    computed_total_nodes = metadata['num_surface_nodes'] + metadata['num_interior_nodes']
    assert computed_total_nodes == metadata['num_nodes'], (
        'Inconsistent number of nodes announced.')
    return metadata


def handle_user() -> argparse.Namespace:
    """
    Read the arguments from the command line.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('ffea_input', type=Path, metavar='FFEA')
    args = parser.parse_args()
    return args


def main():
    args = handle_user()
    serve_ffea(args.ffea_input)


if __name__ == '__main__':
    main()
