Narupa-FFEA — FFEA server for Narupa
====================================

This repository presents a proof of concept for the interaction between FFEA
and Narupa. The `serve_ffea_traj.py` script reads a FFEA trajectory and sends it
in a loop using the Narupa protocol, so it can be visualised with Narupa-iMD.

FFEA simulates proteins as meshes using finite element analysis. It can be
obtained at <https://ffea.bitbucket.io>. See also the article by Solernou
_et al._ in PLOS Computational Biology:
<https://doi.org/10.1371/journal.pcbi.1005897>

Narupa is a framework for interactive molecular dynamics and molecular modeling
in virtual reality. See the article by O'Connor _et al._ in the
Journal of Chemical Physics: <https://doi.org/10.1063/1.5092590>.

Getting started
----------------

The script in this repository requires the core elements of Narupa to be
installed. If you are using conda, the required libraries can be installed with

```bash
conda install -c irl -c conda-forge narupa-core
```

The visualisation requires Narupa iMD, that can be obtained at
<https://gitlab.com/intangiblerealities/narupa-applications/narupa-imd>.

Once the required libraries and softwares are installed, run

```bash
python ./serve_ffea_traj.py your-simulation.ffea
```

The script will read the `.ffea` file as well as the `.ftj` and `.surf` files
that must be referenced in the `.ffea` file. The script acts as a server for
Narupa iMD. Whist the script is running, run Narupa iMD and connect to the
default ports. The simulation should be visible in virtual reality.