# Copyright (c) Intangible Realities Lab, University Of Bristol. All rights reserved.
# Licensed under the GPL. See License.txt in the project root for license information.
import pytest

import serve_ffea_traj
from narupa.trajectory import FrameData


def test_striped_lines():
    input_lines = [
        '   content   \n',
        'hello\r\n',
        '  a line',
        'an  other  line',
    ]
    expected = [
        'content',
        'hello',
        'a line',
        'an  other  line',
    ]
    obtained = list(serve_ffea_traj.stripped_lines(input_lines))
    assert obtained == expected


def test_count_surface_node_per_blob():
    frame = FrameData()
    frame.arrays['residue.id'] = ['A', 'B', 'B', 'C', 'C', 'C']
    expected = [1, 2, 3]
    obtained = serve_ffea_traj.count_surface_node_per_blob(frame)
    assert obtained == expected